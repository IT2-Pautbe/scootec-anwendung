def preisprominute (minuten, preis):
    return preis * minuten

#Preis pro Minute
preis = 0.16


#Eingabe der Minuten im Terminal
minuten = float(input("Wie lange wollen Sie fahren? Bitte geben Sie die Dauer in Minuten an:"))


#Aufruf der Funktion zur Berechnung des Preises pro Minute
preis_pro_minute = preisprominute(minuten, preis)

#Ausgabe der Ergebenisse 
print("Der Preis für die Fahrt beträgt:", preis_pro_minute)